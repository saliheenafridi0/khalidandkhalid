// tailwind.config.js
module.exports = {
  // purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  purge: ["./pages/**/*.js", "./components/**/*.js"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      "2xl": { min: "1535px" },
      // => @media (max-width: 1535px) { ... }
      xl: { max: "1280x" },
      // => @media (max-width: 1280x) { ... }
      lg: { max: "1024px" },
      // => @media (max-width: 1024px) { ... }
      xmd: { max: "801px" },
      // => @media (max-width: 801px) { ... }
      md: { max: "768px" },
      // => @media (max-width: 768px) { ... }
      sm: { max: "600px" },
      // => @media (max-width: 600px) { ... }
      xs: { max: "425px" },
      // => @media (max-width: 425px) { ... }
      xxs: { max: "375px" },
      // => @media (max-width: 375px) { ... }
      xss: { max: "360px" },
    },
    extend: {
      colors: {
        liteblue: "#00A1EB",
        white: "#fff",
        black: "#071C2E",
        litegray: "#949494",
        darkgray: "#212121DE",
        whiteghost: "#F9F9F9",
        red: "#EE0101",
        mGray: '#F3F3F3',
        textGray: '#333333'
      },
      spacing: {
        "10px": "10px",
        "20px": "20px",
        "34px": "34px",
        "40px": "40px",
        "45px": "45px",
        "48px": "48px",
        "50px": "50px",
        "56px": "56px",
        "53px": "53px",
        "70px": "70px",
        "77px": "77px",
        "82px": "82px",
        "99px": "99px",
        "110px": "110px",
        "115px": "115px",
        "129px": "129px",
        "139px": "139px",
        "150px": "150px",
        "181px": "181px",
        "188px": "188px",
        "202px": "202px",
        "232px": "232px",
        "238px": "238px",
        "250px": "250px",
        "257px": "257px",
        "269px": "269px",
        "292px": "292px",
        "310px": "310px",
        "340px": "340px",
        "360px": "360px",
        "435px": "435px",
        "445px": "445px",
        "504px": "504px",
        "643px": "643px",
        "1088px": "1088px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
