import React from "react";

const Customer = ({ text, Customer, image, designation }) =>
{
  return (
    <div className={ `mt-50px w-310px justify-self-center` }>
      <div className="text-sm text-litegray hidden">
        <img src="images/qout1.svg" alt="" className="inline-block w-8 h-8" />
        <span className="mx-5 inline-block text-center p-0">{ text }</span>
        <span className="block text-right">
          <img
            src="images/qout2.svg"
            alt=""
            className="w-8 h-8 inline-block "
          />
        </span>
      </div>
      <img src={ image } alt="" className="w-82px h-82px mx-auto" />
      <h1 className="text-black font-normal text-lg text-center my-10px">
        { Customer }
      </h1>
      <p className="font15 text-darkgray text-center hidden">{ designation }</p>
    </div>
  );
};

export default Customer;
