import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

const Cards = ({ companyname, cityname, jobname, text, onclick, id, slug }) =>
{
  const router = useRouter();

  return (
    <div className="bg-whiteghost w-340px px-5 py-8 mt-5 lg:w-310px sm:w-full">
      <div className="flex items-center">
        <img src="images/kandk.svg" />
        <div className="ml-5">
          <h1 className="text-black font-semibold text-xl">{ companyname }</h1>
          <p className="flex">
            <img src="images/map.svg" />{ " " }
            <span className="text-litegray text-base ml-2">{ cityname }</span>
          </p>
        </div>
      </div>
      <h5 className="text-lg text-black font-semibold mt-5">{ jobname }</h5>
      <p className="text-sm text-litegray mt-2.5">{ text }</p>
      <Link
        href={ `/career-apply/${[slug]}/${[id]}` }
        as={ `/career-apply/${[slug]}/${[id]}` }
      >
        <a className="h-34px w-150px bg-liteblue text-white text-sm mt-3 flex justify-center items-center">
          Apply now
        </a>
      </Link>
    </div>
  );
};

export default Cards;
