import React from "react";

const Button = ({ text, className, onClick }) =>
{
  return (
    <div>
      <button className={ `h-34px ${className}` }>{ text }</button>
    </div>
  );
};

export default Button;
