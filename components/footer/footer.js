import React, { useState, useEffect } from "react";
import Link from "next/link";
import Api from "../../pages/api/api";
const Footer = ({ weoffer, footerLink = [] }) =>
{
  const [footerData, setFooterData] = useState({});
  const [themData, setThemData] = useState({});

  const loadFooterData = async () =>
  {
    try {
      const { data } = await Api.get('get-footer-links')
      setFooterData(data[0])
    } catch (err) {
      console.log(err)
    }
  }

  const loadThemData = async () =>
  {
    try {
      const { data } = await Api.get('get-theme-information')
      setThemData({ ...data })
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() =>
  {
    loadThemData()
    loadFooterData()
  }, []);

  return (
    <div className="bg-black mt-50px pt-110px">
      <div className="customcontainer mx-auto ">
        <div className="flex sm:flex-wrap">
          <div className="w-1/4 sm:w-1/2 sm:mt-3 xs:w-full ">
            <h1 className="text-white font-medium text-lg">Category</h1>
            <ul className="mt-50px">
              { footerData?.category_section?.map((items, index) =>
              {
                return (
                  <li className="text-white my-5 text-sm" key={ index }>
                    <Link href={ items?.footer_link }>
                      { items?.footer_linkname }
                    </Link>
                  </li>
                );
              }) }
            </ul>
          </div>
          <div className="w-1/4 sm:w-1/2 sm:mt-3 xs:w-full ">
            <h1 className="text-white font-medium text-lg">Products</h1>
            <ul className="mt-50px">
              { footerData?.product_section?.map((items, index) =>
              {
                return (
                  <li className="text-white my-5 text-sm" key={ index }>
                    <Link href={ '#' }>
                      { items?.footer_linkname }
                    </Link>
                  </li>
                );
              }) }
            </ul>
          </div>
          <div className="w-1/4 sm:w-1/2 sm:mt-3 xs:w-full ">
            <h1 className="text-white font-medium text-lg">Quick Links</h1>
            <ul className="mt-50px">
              { footerData?.quick_links?.map((items, index) => 
              {
                return (
                  <li className="text-white my-5 text-sm" key={ index }>
                    <Link href={ '#' }>
                      { items?.footer_linkname }
                    </Link>
                  </li>
                );
              }) }
            </ul>
          </div>
          <div className="w-1/4 sm:w-1/2 sm:mt-3 xs:w-full ">
            <img src={ weoffer?.theme_logo } />
            <p className="text-white text-sm mt-50px">
              { weoffer?.theme_about.split(">")[1].split("<")[0] }
            </p>
            <p className="text-white text-sm mt-50px">
              { themData?.theme_copyright ? themData?.theme_copyright : "Copyright 2020 - All right Recived" }
            </p>
          </div>
        </div>
        <div className="flex justify-center my-40px">
          <a href={ `${weoffer?.theme_linkdin}` }>
            <img src="/images/footerin.svg" className="mr-5 cursor-pointer" alt="" />
          </a>
          <a href={ `${weoffer?.theme_instagram}` }>
            <img src="/images/footersk.svg" className="mr-5 cursor-pointer" alt="" />
          </a>
          <a href={ `${weoffer?.theme_facebook}` }>
            <img src="/images/footerfb.svg" className="mr-5 cursor-pointer" alt="" />
          </a>
        </div>
      </div>
      <div className="border-t flex justify-around py-5 xs:flex-wrap xs:justify-between xs:px-3">
        <div className="flex xs:flex-wrap">
          <p className="text-white text-sm">{ themData?.theme_phone ? themData?.theme_phone : "Tel: +92 333 9053362" }</p>
          <p className="text-white text-sm ml-50px xs:ml-4">
            { ` Email:${themData?.theme_email ? themData?.theme_email : "info@kkh.com"}` }
          </p>
        </div>
        <p className="text-white text-sm xs:mt-3">
          { `Address:${themData?.theme_address ? themData?.theme_address : "112, Street 07, Phase 06, Hayatabad, Peshawar"}` }
        </p>
      </div>
    </div>
  );
};




export default Footer;
