import React from "react";

const Pageloader = () =>
{
  return (
    <>
      <div className="pageLoader fixed h-full w-full top-0 left-0 z-50 flex justify-center items-center">
        <div className="lds-roller">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </>
  );
};

export default Pageloader;
