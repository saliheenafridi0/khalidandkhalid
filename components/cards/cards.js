import React from "react";
import Link from "next/link";

const Cards = ({
  className,
  title,
  text,
  img,
  btntext,
  btnclass,
  href = "#",
  as = "#",
}) =>
{
  return (
    <div className={ `${className}` }>
      <img src={ img } alt="" className="h-181px object-contain w-full" />
      <h4 className="text-base text-black font-semibold my-2.5">{ title }</h4>
      <p className="text-litegray text-base">{ text }</p>
      <Link href={ href } as={ as }>
        <button
          className={ `h-34px w-99px border border-liteblue  text-sm mt-3 ${btnclass}` }
        >
          { btntext }
        </button>
      </Link>
    </div>
  );
};

export default Cards;
