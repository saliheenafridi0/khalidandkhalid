import React, { useEffect, useState } from "react";
import Link from "next/link";
import Api from "../../pages/api/api";
import Pageloader from "../pageloader/pageloader";

const Topheader = () =>
{
  const loadCompanies = async () =>
  {
    setPageLoader(true)
    const { data } = await Api.get("/get-companies");
    setCompinesLogo([...data])
    setPageLoader(false)

  };

  const loadThemData = async () =>
  {
    try {
      const { data } = await Api.get('get-theme-information')
      setThemData({ ...data })
    } catch (error) {
      console.log(error)
    }
  }

  const [compinesLogo, setCompinesLogo] = useState([]);
  const [themData, setThemData] = useState({});

  const [pageLoader, setPageLoader] = useState(false);
  const [nav, setnav] = useState(false);


  useEffect(() =>
  {
    loadThemData()
    loadCompanies()
  }, []);

  return (
    <>
      { pageLoader ? <Pageloader /> : null }
      <div className="bg-liteblue relative h-40px xmd:hidden">
        <div className="customcontainer  mx-auto flex justify-between items-center h-full ">
          <p className="text-white text-xs uppercase">
            New era of locally assembled vehicle's
          </p>
          <div className="flex justify-between z-10">
            <div className="flex items-center px-4">
              <img src="/images/headphone.svg" alt="" />
              <p className="text-white text-base mt-1 ml-1">{ themData?.theme_phone ? themData?.theme_phone : "+92 333 9053362" }</p>
            </div>
            <div className="flex items-center">
              <img src="/images/email.svg" alt="" />
              <p className="text-white text-base mt-1 ml-1">{ themData?.theme_email ? themData?.theme_email : "info@kandk.com" }</p>
            </div>
          </div>
        </div>
        <div className="bg-black h-full right-0 top-0 absolute clippathright w-2/5 2xl:w-1/2 "></div>
      </div>

      {/* Logo section */ }
      <div className="flex items-center justify-between py-4 px-40px xmd:px-20px 2xl:px-0 2xl:w-1088px 2xl:mx-auto">
        <div>
          <Link href="/">
            <img src={ themData?.theme_logo } className="cursor-pointer h-28" />
          </Link>
        </div>
        <div className="xmd:hidden">
          <img src="/images/logo2.svg" />
          <div className="flex justify-end mt-3 ">
            { compinesLogo?.map((ele, index) =>
            {
              return (
                <a target="_blank" rel="noopener noreferrer" href={ `${ele.company_link}` } key={ index }>
                  <img
                    key={ index }
                    src={ ele?.company_logo }
                    alt=""
                    className="mx-1 h-48px w-48px object-cover cursor-pointer"
                  />
                </a>
              );
            }) }
          </div>
        </div>
        <div className="hidden xmd:block">
          <button
            onClick={ () =>
            {
              setnav(!nav);
            } }
          >
            <img src="/images/bar.svg" />
          </button>
        </div>
      </div>
      <nav
        className={ `xmd:bg-whiteghost xmd:fixed xmd:z-30 xmd:w-full xmd:h-full xmd:top-0
         ${nav ? "transform100" : "transform0"} ` }
      >
        <div className="hidden xmd:flex justify-between px-5 pt-8 ">
          <img src="/images/logo.svg" className="xxs:w-238px" />
          <button onClick={ () => setnav(false) }>
            <img src="/images/cro.svg" />
          </button>
        </div>
        <ul className="my-34px flex justify-center xmd:mb-4 xmd:flex-col ">
          <li className="text-black text-base px-4 xmd:py-3 xmd:border-b xmd:px-8">
            <Link href="/">Home</Link>
          </li>
          <li className="text-black text-base px-4 xmd:py-3 xmd:border-b xmd:px-8">
            <Link href="/aboutus">About</Link>
          </li>
          <li className="text-black text-base px-4 xmd:py-3 xmd:border-b xmd:px-8">
            <Link href="/productlist">Products</Link>
          </li>
          <li className="text-black text-base px-4 xmd:py-3 xmd:border-b xmd:px-8">
            <Link href="/services">Services</Link>
          </li>
          <li className="text-black text-base px-4 xmd:py-3 xmd:border-b xmd:px-8">
            <Link href="/career">Career</Link>
          </li>
          <li className="text-black text-base px-4 xmd:py-3 xmd:border-b xmd:px-8">
            <Link href="/contact">Contact us</Link>
          </li>
        </ul>
        <div className="hidden xmd:flex sm:block p-5">
          <div className="flex items-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="23.971"
              height="20"
              viewBox="0 0 23.971 20"
            >
              <g
                id="_01_align_center"
                data-name="01 align center"
                transform="translate(-0.014 -2)"
              >
                <path
                  id="Path_8"
                  data-name="Path 8"
                  d="M21,12.424V11A9,9,0,0,0,3,11v1.424A5,5,0,0,0,5,22H7V12H5V11a7,7,0,0,1,14,0v1H17v8H13v2h6a5,5,0,0,0,2-9.576ZM5,20a3,3,0,0,1,0-6Zm14,0V14a3,3,0,0,1,0,6Z"
                  fill="#071C2E"
                />
              </g>
            </svg>
            <p className="text-black text-base font-semibold mt-1 ml-3">
              +92 333 9053362
            </p>
          </div>
          <div className="flex items-center ml-20 sm:ml-0 sm:mt-4">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="25"
              height="20"
              viewBox="0 0 25 20"
            >
              <g
                id="_01_align_center"
                data-name="01 align center"
                transform="translate(0 -2)"
              >
                <path
                  id="Path_9"
                  data-name="Path 9"
                  d="M22,2H3A3,3,0,0,0,0,5V22H25V5A3,3,0,0,0,22,2ZM3,4H22a1,1,0,0,1,1,1v.667l-8.878,6.879a3.007,3.007,0,0,1-4.244,0L2,5.667V5A1,1,0,0,1,3,4ZM2,20V7.5l6.464,6.46a5.007,5.007,0,0,0,7.072,0L23,7.5V20Z"
                  fill="#071C2E"
                />
              </g>
            </svg>

            <p className="text-black text-base font-semibold mt-1 ml-3">
              info@kandk.com
            </p>
          </div>
        </div>
      </nav>
    </>
  );
};



export default Topheader;










