import { useRouter } from "next/router";
import Link from "next/link";

const Servicecard = ({
  whiteimg,
  blackimg,
  img,
  title,
  servicename,
  text,
  id,
  slug,
}) =>
{
  const router = useRouter();
  // console.log(text.split(">")[1].split("<")[0]);
  return (
    <div className="border w-310px px-5 py-7 mb-10 rounded-xl xmd:w-238px service_card">
      {/* <img src={blackimg} className="blackimg" />
      <img src={whiteimg} className="whiteimg" /> */}
      <img src={ img } className="border h-64 w-full rounded-xl" />
      <h5 className="text-liteblue text-sm mt-6">{ title }</h5>
      <h1 className="text-lg text-black font-semibold mt-2.5">{ servicename }</h1>
      <p className="text-base text-litegray mt-4 text-justify">
        { text.split(">")[1].split("<")[0] }
      </p>
      <Link href={ `/servicesdetails/${[slug]}/${[id]}` }>
        <a
          className={ `h-34px w-99px border border-liteblue hidden text-sm mt-3 text-liteblue flex justify-center items-center` }
        >
          Read more
        </a>
      </Link>
      {/* <a
        className={`h-34px w-99px border border-liteblue  text-sm mt-3 text-liteblue`}
        onClick={() =>
          router.push({ pathname: "/servicedetails", query: { id } })
        }
      >
        
      </a> */}
    </div>
  );
};

export default Servicecard;
