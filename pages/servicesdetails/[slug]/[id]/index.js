import React, { useEffect, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useRouter } from "next/router";
import Pageloader from "../../../../components/pageloader/pageloader";
import Api from "../../../api/api";

const Index = () =>
{
  const router = useRouter();
  const [pageloader, setPageoader] = useState(false);
  const [servicedet, setServivedet] = useState();
  const serviesid = router?.query?.id;
  const servicedets = async () =>
  {
    try {
      setPageoader(true);
      const { data } = await Api.get(`/service-details/service-2/${serviesid}`);
      setServivedet({ ...data });
      setPageoader(false);
    } catch (error) {
      setPageoader(false);
      alert(error);
    }
  };
  useEffect(() =>
  {
    servicedets();
  }, []);

  const Prevarrow = ({ onClick }) =>
  {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-prev"
        onClick={ onClick }
      >
        <img src="/images/left1.svg" className="transform rotate-180" />
      </button>
    );
  };
  const Nextarrow = ({ onClick }) =>
  {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-next"
        onClick={ onClick }
      >
        <img src="/images/right1.svg" />
      </button>
    );
  };
  var silder2 = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    nextArrow: <Nextarrow />,
    prevArrow: <Prevarrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
        },
      },
    ],
  };
  return (
    <>
      { pageloader ? <Pageloader /> : null }
      <div className="bg_banner servicedet_bg px-129px py-20 lg:px-40px lg:py-5 xmd:px-5">
        <img src={ servicedet?.service_cover } className="self-start" />
        <p className="text-sm text-white mt-8 sm:mt-2.5">
          { servicedet?.service_excerpt }
        </p>
        <h1 className="text-white text-3xl font-semibold mt-5 sm:mt-2.5">
          { servicedet?.service_name }
        </h1>
      </div>
      <div className="customcontainer mx-auto mt-10 flex sm:flex-wrap">
        <div className="w-3/4 sm:w-full pr-7 sm:pr-0">
          <Slider { ...silder2 } className="service_slider">
            <div>
              <img src="/images/sd.png" className="w-full" />
            </div>
            <div>
              <img src="/images/sd.png" className="w-full" />
            </div>
          </Slider>
          <div>
            <p className="text-litegray text-sm mt-7">
              { servicedet?.service_details.split(">")[1].split("<")[0] }
            </p>
          </div>
        </div>
        <div className="w-1/4 sm:w-full sm:mt-8">
          <div className="bg-whiteghost px-7 py-5 lg:px-3">
            <h1 className="text-xl text-black font-semibold">All services</h1>
            <ul className="mt-4">
              <li className="border-b py-2.5 text-base text-litegray cursor-pointer transition duration-300 ease-in-out hover:text-liteblue">
                Expert mechanical engineer
              </li>
              <li className="border-b py-2.5 text-base text-litegray cursor-pointer transition duration-300 ease-in-out hover:text-liteblue">
                Industrial engineer
              </li>
              <li className="border-b py-2.5 text-base text-litegray cursor-pointer transition duration-300 ease-in-out hover:text-liteblue">
                Automotive manufacturing
              </li>
              <li className="border-b py-2.5 text-base text-litegray cursor-pointer transition duration-300 ease-in-out hover:text-liteblue">
                Mechanical industry
              </li>
              <li className="border-b py-2.5 text-base text-litegray cursor-pointer transition duration-300 ease-in-out hover:text-liteblue">
                Industrial engineer
              </li>
            </ul>
          </div>
          <div className="bg-whiteghost px-7 py-5 mt-7 lg:px-3">
            <h1 className="text-xl text-black font-semibold">
              Download broacher
            </h1>
            <ul className="mt-4">
              <li className="border-b py-2.5 flex text-base text-litegray">
                <img src="/images/pdf1.svg" />
                <span className="ml-2">Services Abc.pdf</span>
              </li>
              <li className="border-b py-2.5 flex text-base text-litegray">
                <img src="/images/pdf1.svg" />
                <span className="ml-2">Mechanical.pdf</span>
              </li>
              <li className="border-b py-2.5 flex text-base text-litegray">
                <img src="/images/pdf1.svg" />
                <span className="ml-2">Details about service.pdf</span>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="mt-7 pl-129px w-3/4 lg:pl-40px xmd:pl-20px sm:w-full">
        <h1 className="text-3xl font-bold  text-black">
          Get in <span className="text-liteblue "> touch </span>
        </h1>
        <p className="text-litegray text-sm mt-6">
          magna. Lorem ipsum dolor sit amet, lorem nibh lectus urna arcu, lorem
          erat semper, auctor . molestie ut. Elit massa dui, leo enim magna.
          Elit massa dui, leo enim magna. Lorem ipsum dolor sit amet, lorem nibh
          lectus urna arcu, lorem erat semper, auctor . Molestie ut. Elit massa
          dui, leo enim magna. Elit massa dui, leo enim magna. Lorem ipsum dolor
          sit amet, lorem nibh lectus urna arcu,
        </p>
        <div className="mt-8 flex sm:flex-wrap sm:flex-col">
          <div className=" flex items-center">
            <img src="/images/phone.svg" />
            <div className="ml-6">
              <h3 className="text-lg text-black font-semibold">
                Call us anytime
              </h3>
              <p className="text-sm text-litegray mt-3">+92 333 9053362</p>
            </div>
          </div>
          <div className=" flex items-center ml-24 sm:ml-0 sm:mt-6">
            <img src="/images/email2.svg" />
            <div className="ml-6">
              <h3 className="text-lg text-black font-semibold">Email us</h3>
              <p className="text-sm text-litegray mt-3">contact@kkh.com</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Index;
