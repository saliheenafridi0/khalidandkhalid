import { useRouter } from "next/router";
import React, { useState, useEffect } from "react";
import Api from "../../../api/api";
import parse from "html-react-parser";

function Index ()
{
  const [blogDetailsData, setBlogDetailsData] = useState({});
  const [htmlString, setHtmlString] = useState("");
  const router = useRouter();

  const loadBlogDetails = async () =>
  {
    try {
      const { data } = await Api.get(
        `/get-blog-details/${router?.query?.slug}/${router?.query?.code}`
      );
      setBlogDetailsData({ ...data });
      setHtmlString(data.blog_details);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() =>
  {
    loadBlogDetails();
  }, []);

  return (
    <section className="customcontainer mx-auto mt-6">
      <img src={ blogDetailsData?.blog_image } />
      <h1 className="text-darkgray text-xl font-semibold my-8">
        { blogDetailsData?.blog_title }
      </h1>
      <div>{ parse(htmlString) }</div>
    </section>
  );
}

export default Index;
