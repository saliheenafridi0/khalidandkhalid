import React, { useState, useRef, Fragment, useEffect } from "react";
import { Dialog, Transition, Menu } from "@headlessui/react";
import { useRouter } from "next/router";
import Api from "./api/api";


const Applyjob = () =>
{
  const [open, setOpen] = useState(false);
  const [jdetails, setJdetails] = useState();
  const cancelButtonRef = useRef(null);
  const router = useRouter();

  const jobdetail = async () =>
  {
    const job_id = router?.query?.id;
    try {
      const { data } = await Api.get(`/job-detailes/${job_id}`);
      setJdetails({ ...data });
    } catch (error) { }
  };

  useEffect(() =>
  {
    jobdetail();
  }, []);
  
  return (
    <>
      {/* start mapmodal */ }
      <Transition.Root show={ open } as={ Fragment }>
        <Dialog
          as="div"
          className="fixed z-10 inset-0 overflow-y-auto"
          initialFocus={ cancelButtonRef }
          onClose={ setOpen }
        >
          <div className="flex items-start justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <Transition.Child
              as={ Fragment }
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */ }
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={ Fragment }
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <div className="inline-block align-middle pb-3 modal-size bg-white text-left overflow-hidden shadow-xl transform transition-all">
                <div className="flex justify-between px-4 py-6 bg-black">
                  <h1 className="text-white text-xl font-semibold">
                    Apply for: Industrial Engineer
                  </h1>
                  <img
                    src="images/cross.svg"
                    className="cursor-pointer"
                    onClick={ () => setOpen(false) }
                  />
                </div>
                <form className="px-40px sm:px-4">
                  <div className="my-1 ">
                    <label className="text-litegray text-sm my-2 block">
                      Name
                    </label>
                    <input
                      type="text"
                      placeholder="Enter your full name"
                      className="border text-litegray pl-2 h-50px w-435px sm:w-full"
                    />
                  </div>
                  <div className="my-1 ">
                    <label className="text-litegray my-2 text-sm block">
                      Email
                    </label>
                    <input
                      type="text"
                      placeholder="Enter your email address"
                      className="border text-litegray pl-2 h-50px w-435px sm:w-full"
                    />
                  </div>
                  <div className="my-1 ">
                    <label className="text-litegray my-2 text-sm block">
                      Mobile number
                    </label>
                    <input
                      type="text"
                      placeholder="Enter your mobile number"
                      className="border text-litegray pl-2 h-50px w-435px sm:w-full"
                    />
                  </div>
                  <div className=" my-1">
                    <label className="text-litegray my-2 text-sm block">
                      Cover letter
                    </label>
                    <textarea
                      placeholder="Write here"
                      className="border text-litegray p-2 h-110px w-full resize-none "
                    />
                  </div>
                  <div className="my-1 ">
                    <label className="inline-block cursor-pointer">
                      <input type="file" className="hidden" />
                      <button className="text-litegray text-xs border bg-whiteghost w-99px h-6 mr-3">
                        Choose file
                      </button>
                      <span className="text-litegray text-sm">
                        Upload your CV
                      </span>
                    </label>
                  </div>
                  <div>
                    <button className="h-40px w-full border bg-liteblue text-white text-sm mt-3">
                      Send message
                    </button>
                  </div>
                </form>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>
      {/* End Madmodal */ }

      <div className="bg_banner carrer_bg px-129px py-20 lg:px-40px lg:py-5 xmd:px-5">
        <h1 className="text-white text-3xl font-semibold">
          K&K Group of Companies
        </h1>
        <p className="flex text-white mt-3">
          <img src="images/wmap.svg" />{ " " }
          <span className=" text-base ml-2">{ jdetails?.job_location }</span>
        </p>
        <h3 className="text-white font25 mt-2">{ jdetails?.job_title }</h3>
      </div>
      <div className="customcontainer mx-auto mt-12 flex flex-wrap">
        <div className="bg-whiteghost h-238px p-3 w-1/4 xmd:h-250px sm:w-full">
          <ul>
            <li className="flex justify-between items-center border-b py-3 xmd:py-2.5">
              <strong className="text-lg text-black">Last date</strong>
              <p className="text-litegray text-sm">{ jdetails?.job_lastdate }</p>
            </li>
            <li className="flex justify-between items-center border-b py-3 xmd:py-2.5">
              <strong className="text-lg text-black">Location</strong>
              <p className="text-litegray text-sm">{ jdetails?.job_location }</p>
            </li>

            <li className="flex justify-between items-center border-b py-3 xmd:py-2.5">
              <strong className="text-lg text-black">Type</strong>
              <p className="text-litegray text-sm">{ jdetails?.job_type }</p>
            </li>
            <li className="flex justify-between items-center border-b py-3 xmd:py-2.5">
              <strong className="text-lg text-black">Employer</strong>
              <p className="text-litegray text-sm">K&K Abc</p>
            </li>
          </ul>
        </div>
        <div className="w-3/4 pl-8 sm:w-full sm:p-0 sm:mt-8">
          <p className="text-litegray text-sm">{ jdetails?.job_detail }</p>
        </div>
        <div className="w-full text-center">
          <button
            className="h-77px w-269px  bg-liteblue text-white text-lg mt-3"
            onClick={ () => setOpen(true) }
          >
            <span className=" inline-block">Apply for this job</span>
            <img src="images/send.svg" className="ml-2 inline-block" />
          </button>
        </div>
      </div>
      <div className="customcontainer mx-auto mt-16 flex justify-between flex-wrap">
        <div className="bg-whiteghost w-504px p-8 lg:w-445px xmd:w-360px sm:w-full">
          <h6 className="text-black text-lg font-semibold">Features</h6>

          <p className="text-litegray text-sm mt-3 ml-4">
            { jdetails?.job_features }
          </p>
        </div>
        <div className="bg-whiteghost w-504px p-8 lg:w-445px xmd:w-360px sm:w-full sm:mt-8">
          <h6 className="text-black text-lg font-semibold">Benefits</h6>
          <p className="text-litegray text-sm mt-3 ml-4">
            { jdetails?.job_benefits }
          </p>
        </div>
      </div>
    </>
  );
};

export default Applyjob;
