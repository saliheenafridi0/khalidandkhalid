import React, { useEffect, useState } from "react";
import Pageloader from "../components/pageloader/pageloader";
import Api from "./api/api";

const Contact = () =>
{
  const [selectedTab, setSelectedTab] = useState();
  const [branchList, setBaranchList] = useState([]);
  const [branchName, setBaranchName] = useState();
  const [branchData, setBranchData] = useState({});
  const [pageLoader, setPageLoader] = useState(false);
  const loadContact = async () =>
  {
    try {
      setPageLoader(true);
      const { data } = await Api.get("/contact-information");
      setBaranchList([...data[0]?.branches]);
      const {
        contact_title,
        contact_branch,
        contact_address,
        contact_phone,
        contact_mobile,
        contact_fax,
        contact_email,
      } = data[0]?.firstbranch;

      setBranchData({
        contact_title,
        contact_branch,
        contact_address,
        contact_phone,
        contact_mobile,
        contact_fax,
        contact_email,
      });
      setPageLoader(false);
    } catch (error) {
      setPageLoader(false);
      console.log(error);
    }
  };
  const loadbranch = async () =>
  {
    try {
      setPageLoader(true);
      const { data } = await Api.get(`/contact-information/${branchName}`);
      const {
        contact_title,
        contact_branch,
        contact_address,
        contact_phone,
        contact_mobile,
        contact_fax,
        contact_email,
      } = data;

      setBranchData({
        contact_title,
        contact_branch,
        contact_address,
        contact_phone,
        contact_mobile,
        contact_fax,
        contact_email,
      });
      setPageLoader(false);
    } catch (error) {
      setPageLoader(false);

      console.log(error);
    }
  };
  useEffect(() =>
  {
    loadContact();
  }, []);
  useEffect(() =>
  {
    loadbranch();
  }, [branchName]);
  return (
    <>
      { pageLoader ? <Pageloader /> : null }
      <div className="bg_banner contact_bg px-129px py-20 lg:px-40px lg:py-5 xmd:px-5 ">
        <h1 className="text-white text-3xl font-semibold hidden">Contact us</h1>
        <h3 className="text-white font22 mt-5 hidden">
          Kahlid & Khalid in Peshawar, Islamabad, Lahore, Rawalpindi, Karachi
        </h3>
        <p className="text-white text-base font-light my-3 hidden">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
        </p>
        <p className="text-white text-base font-light my-3 sm:hidden hidden">
          ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet,
          consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
          exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.Lorem ipsum dolor sit amet, consectetur adipisicing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
          enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
          ut aliquip ex ea commodo consequat.
        </p>
        <div className="sm:bg-white">
          <div className="customcontainer mx-auto py-14 px-20">
            <div className="flex flex-wrap">
              <div className="bg-black w-1/5 sm:w-full">
                { branchList?.map((items, index) =>
                {
                  return (
                    <div className=" border-b py-1" key={ index }>
                      <button
                        className={ ` 
                ${selectedTab === items
                            ? "bg-white  text-black"
                            : "bg-black text-white "
                          } 
                 py-1 pl-8 text-left uppercase text-base w-full `}
                        onClick={ () =>
                        {
                          setSelectedTab(items);
                          setBaranchName(items?.contact_branch);
                        } }
                      >
                        { items?.contact_branch }
                      </button>
                    </div>
                  );
                }) }
              </div>
              <div className="w-4/5 pl-8 sm:w-full sm:pl-0 sm:mt-6">
                <div>
                  <p className="text-white text-base font-semibold py-2.5 px-5 sm:px-0">
                    { branchData?.contact_title }
                  </p>
                  <div className="py-2.5  px-5 mt-4 sm:px-0">
                    <p className="text-white text-base font-semibold ">
                      The Official Khalid & Khalid Dealer in{ " " }
                      { branchData?.contact_branch } ( Retailer )
                    </p>
                    <div>
                      <table className="w-full mt-10">
                        <tr>
                          <td className="text-white text-sm font-semibold">
                            Address
                          </td>
                          <td className="text-white text-sm">
                            { branchData?.contact_address }
                          </td>
                        </tr>
                        <tr>
                          <td className="text-white text-sm font-semibold">
                            Telephone
                          </td>
                          <td className="text-white text-sm">
                            { branchData?.contact_phone }
                          </td>
                        </tr>
                        <tr>
                          <td className="text-white text-sm font-semibold">
                            Mobile
                          </td>
                          <td className="text-white text-sm">
                            { branchData?.contact_mobile }
                          </td>
                        </tr>
                        <tr>
                          <td className="text-white text-sm font-semibold">
                            Fax
                          </td>
                          <td className="text-white text-sm">
                            { branchData?.contact_fax }
                          </td>
                        </tr>
                        <tr>
                          <td className="text-white text-sm font-semibold">
                            Email
                          </td>
                          <td className="text-white text-sm">
                            { branchData?.contact_email }
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="customcontainer mx-auto mt-8">
        <h1 className="text-3xl font-bold text-black">
          Get in
          <span className="text-black "> touch</span>
        </h1>
        <p className="text-litegray text-sm my-8 hidden">
          Lorem ipsum dolor sit amet, lorem nibh lectus urna arcu, lorem erat
          semper, auctor suspendisse quisque molestie ut. Elit massa dui, leo
          enim magna. Lorem ipsum dolor sit amet, lorem nibh lectus urna arcu,
          lorem erat semper, auctor suspendisse quisque.
        </p>
        <form className=" flex flex-wrap px-20 lg:px-0">
          <div className="w-1/2 my-1 sm:w-full">
            <label className="text-litegray text-sm my-2 block">
              Name <span className="text-red">*</span>
            </label>
            <input
              type="text"
              placeholder="Enter your full name"
              className="border text-litegray pl-2 h-50px w-435px xmd:w-340px sm:w-full"
            // onChange={(e) => {
            //   let user = e.target.value;
            // }}
            />
          </div>
          <div className="w-1/2 my-1 sm:w-full">
            <label className="text-litegray my-2 text-sm block">
              Email <span className="text-red">*</span>
            </label>
            <input
              type="text"
              placeholder="Enter your email address"
              className="border text-litegray pl-2 h-50px w-435px xmd:w-340px sm:w-full"
            />
          </div>
          <div className="w-1/2 my-1 sm:w-full">
            <label className="text-litegray my-2 text-sm block">
              Mobile number
            </label>
            <input
              type="text"
              placeholder="Enter your mobile number"
              className="border text-litegray pl-2 h-50px w-435px xmd:w-340px sm:w-full"
            />
          </div>
          <div className="w-1/2 my-1 sm:w-full">
            <label className="text-litegray my-2 text-sm block">
              Country <span className="text-red ">*</span>
            </label>
            <select className="border text-litegray  bg-transparent  pl-2 h-50px w-435px xmd:w-340px sm:w-full">
              <option>Select country</option>
            </select>
          </div>
          <div className="w-1/2 my-1 sm:w-full">
            <label className="text-litegray my-2 text-sm block">
              Interested in <span className="text-red ">*</span>
            </label>
            <select className="border text-litegray bg-transparent pl-2 h-50px w-435px xmd:w-340px sm:w-full">
              <option>Interested in </option>
            </select>
          </div>
          <div className="w-1/2 my-1 sm:w-full">
            <label className="text-litegray my-2 text-sm block">Company</label>
            <input
              type="text"
              placeholder="Enter your company name"
              className="border text-litegray pl-2 h-50px w-435px xmd:w-340px sm:w-full"
            />
          </div>
          <div className="w-full my-1 sm:w-full">
            <label className="text-litegray my-2 text-sm block">
              Details <span className="text-red">*</span>
            </label>
            <textarea
              placeholder="Enter your company name"
              className="border text-litegray p-2 h-110px w-full resize-none "
            />
          </div>
          <div>
            <button className="h-34px w-150px border bg-liteblue text-white text-sm mt-3">
              Send message
            </button>
          </div>
        </form>
      </div>
      <div>
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13396.624833226391!2d71.6304345!3d32.9204709!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcdebf99353327774!2sKhalid%20%26%20Khalid%20Holdings!5e0!3m2!1sen!2s!4v1645698762310!5m2!1sen!2s"
          className="mt-5 w-full h-445px"
          loading="lazy"
        ></iframe>
      </div>
    </>
  );
};

export default Contact;
