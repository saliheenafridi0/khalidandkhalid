import React, { useEffect, useState } from "react";
import Pageloader from "../components/pageloader/pageloader";
import Servicecard from "../components/servicescard/servicecard";
import Api from "./api/api";

const Services = () =>
{
  const [service, setService] = useState([]);
  const [pageLoader, setPageloader] = useState(false);

  const serviceList = async () =>
  {
    try {
      setPageloader(true);
      const { data } = await Api.get("/services-list");
      setService([...data]);
      setPageloader(false);
    } catch (error) {
      setPageloader(false);
      alert(error);
    }
  };

  useEffect(() =>
  {
    serviceList();
  }, []);

  return (
    <>
      { pageLoader ? <Pageloader /> : null }
      <div className="bg_banner service_bg px-129px py-20 lg:px-40px lg:py-5 xmd:px-5">
        <h1 className="text-white text-3xl font-semibold ">Service </h1>
        <p className="text-white text-lg w-3/4 text-justify font-light my-3 sm:hidden">
          At Khalid and Khalid Holdings we are committed to keep our Trucks, Trailers & Buses operating in optimal conditions. We bring our customers a unique service exeprience through our
          sister company Rawal Industrial Equipment (RIE), where we strive to achieve maximum uptime and ensuring to provide quick and timely
          services along with regular maintenance facilities.
        </p>
        <p className="text-white text-base font-light my-3 hidden sm:hidden">
          ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet,
          consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
          exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.Lorem ipsum dolor sit amet, consectetur adipisicing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
          enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
          ut aliquip ex ea commodo consequat.
        </p>
      </div>
      <div className="customcontainer mx-auto mt-12 grid grid-cols-3 gap-x-6 my-6  sm:grid-cols-1 sm:gap-x-0">
        { service.map((ele, index) =>
        {
          return (
            <Servicecard
              key={ index }
              id={ ele.service_id }
              img={ ele.service_cover }
              title={ ele.service_excerpt }
              servicename={ ele.service_name }
              text={ ele.service_details }
              slug={ ele.service_slug }
            />
          );
        }) }

      </div>
    </>
  );
};

export default Services;
