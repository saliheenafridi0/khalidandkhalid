import axios from "axios";

const Api = axios.create({
  baseURL: "https://panel.khalidandkhalidholdings.com.pk/api",
});

export default Api;
