import React, { useEffect, useState } from "react";
import Pageloader from "../components/pageloader/pageloader";
import Rolecard from "../components/rolecard/cards";
import Api from "./api/api";

const Carreer = () =>
{
  const [pageLoader, setPageloader] = useState(false);
  const [joblist, setJoblist] = useState([]);
  const joblistapi = async () =>
  {
    try {
      setPageloader(true);
      const { data } = await Api.get("/jobs-list");
      setJoblist([...data]);
      setPageloader(false);
    } catch (error) {
      setPageloader(true);
      alert("Error");
    }
  };
  useEffect(() =>
  {
    joblistapi();
  }, []);

  return (
    <>
      { pageLoader ? <Pageloader /> : null }
      <div className="bg_banner carrer_bg px-129px py-20 lg:px-40px lg:py-5 xmd:px-5">
        <h1 className="text-white text-3xl font-semibold">Careers</h1>
        <h3 className="text-white font22 mt-5 hidden">
          It's your career, make it better
        </h3>
        <p className="text-white text-base font-light my-3 hidden">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
        </p>
        <p className="text-white text-lg font-light my-3 sm:hidden">
          Enjoy a rewarding career by working for Khalid & Khalid Holdings and be a part of the larger picture. “A career with Khalid & Khalid Holdings is full of challenges, success and tremendous responsibilities”.
        </p>
        <p className="text-white text-base font-light my-3 sm:hidden">
          Send or mail your resume at the following address
        </p>
      </div>
      <div className="customcontainer mx-auto mt-12">
        <h1 className="text-3xl text-center font-bold text-black">
          We bulid
          <span className="text-black "> careers</span>
        </h1>
        <p className="text-center text-litegray text-sm mt-4 hidden">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet,
          consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
          exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </p>
        <img src="images/career/career.png" className="mt-4 rounded" />
      </div>
      <div className="customcontainer mx-auto mt-12">
        <h1 className="text-3xl text-center font-bold text-black">
          Open
          <span className="text-black "> roles</span>
        </h1>
        <div className="grid grid-cols-3 gap-x-6 my-6    sm:grid-cols-1 sm:gap-x-0">
          { joblist.map((ele, index) =>
          {
            return (
              <Rolecard
                key={ index }
                companyname="Khalid & Khalid"
                cityname={ ele.job_location }
                jobname={ ele.job_title }
                // text={ele.job_detail}
                id={ ele.job_id }
                slug={ ele.job_slug }
              />
            );
          }) }
        </div>
      </div>
    </>
  );
};

export default Carreer;
