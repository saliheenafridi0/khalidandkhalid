import React, { useState, useRef, Fragment, useEffect } from "react";
import { Dialog, Transition, Menu } from "@headlessui/react";
import Card from "../components/cards/cards";
import Customer from "../components/customercard/customer";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Api from "./api/api";
import { useRouter } from "next/router";
import Link from "next/link";
import Pageloader from "../components/pageloader/pageloader";
import ReactPlayer from "react-player";
import parse from "html-react-parser";
import Head from "next/head"

export default function Home ({
  blogCardsList,
  cardsList,
  eventlist,
  categoierlist,
  sliderdata,
  weoffer,
  videoContents,
  themData,
  reviews,
  themContents
})
{
  const Prevarrow = ({ onClick }) =>
  {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-prev"
        onClick={ onClick }
      >
        <img src="images/left1.svg" className="transform rotate-180" />
      </button>
    );
  };
  const loadCompanies = async () =>
  {
    const { data } = await Api.get("/get-companies");
    setCompinesLogo([...data])

  };
  const Nextarrow = ({ onClick }) =>
  {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-next"
        onClick={ onClick }
      >
        <img src="images/right1.svg" />
      </button>
    );
  };
  var silder1 = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    nextArrow: <Nextarrow />,
    prevArrow: <Prevarrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
        },
      },
    ],
  };
  const list = [
    { name: "Dedicated" },
    { name: "Ethical" },
    { name: "Reliable" },
    { name: "Disciplined" },
    { name: "Integrity" },
    { name: "Punctual" },
    { name: "Professional" },
    { name: "Responsible" },
  ];
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const [compinesLogo, setCompinesLogo] = useState([])
  const cancelButtonRef = useRef(null);

  const [pageloader, setPageloader] = useState(false);
  const [selectedTab, setSelectedTab] = useState({});
  const [cards, setcards] = useState("");
  const [selectedCategoryImages, setSelectedCategoryImages] = useState();
  const [selectedCategoryId, setSelectedCaetgoryId] = useState(null);
  const [productsList, setProductsList] = useState([]);
  const [userName, setUserName] = useState();
  const [userEmail, setUserEmail] = useState();
  const [userMassage, setUserMassage] = useState();
  const [isSubCategoryOpened, setIsSubCategoryOpened] = useState(false);
  const [selectedSubCategoriesList, setSelectedSubCategoriesList] = useState([]);
  const [selectedSubCategoryId, setSelectedSubCategoryId] = useState(null);

  const submitForm = async () =>
  {
    setPageloader(true);
    const payload = {
      email: userEmail,
      name: userName,
      message: userMassage,
    };

    try {
      const { data } = await Api.post("/contact-us", payload);
      setPageloader(false);
      if (data?.msgErr) {
        alert(data?.msgErr);
      } else {
        alert("Message sent successfully.");
      }
    } catch (error) {
      alert("An error has occurred.");
      setPageloader(false);
    }
  };

  const loadProductsList = async () =>
  {
    try {
      const { data } = await Api.get(
        `/category-wise-products/${selectedCategoryId}/${selectedSubCategoryId}`
      );
      setProductsList([...data]);
      setPageloader(false);
    } catch (error) {
      setPageloader(false);
      console.log(error);
    }
  };

  useEffect(() =>
  {
    setSelectedCaetgoryId(categoierlist[0]?.category_id);
    setSelectedTab(categoierlist[0]);
    loadCompanies()
  }, []);

  useEffect(() =>
  {
    if (selectedCategoryId) {
      loadProductsList();
    }
  }, [selectedCategoryId, selectedSubCategoryId]);

  return (
    <>
      <Head>
        <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png" />
      </Head>
      {/* start mapmodal */ }
      <Transition.Root show={ open } as={ Fragment }>
        <Dialog
          as="div"
          className="fixed z-10 inset-0 overflow-y-auto"
          initialFocus={ cancelButtonRef }
          onClose={ setOpen }
        >
          <div className="flex items-start justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <Transition.Child
              as={ Fragment }
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */ }
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={ Fragment }
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <div className="inline-block align-middle pb-3 vedio__modal h-435px bg-white text-left overflow-hidden shadow-xl transform transition-all">
                <button
                  className="h-7 w-7 ml-auto block my-3 mr-2"
                  onClick={ () => setOpen(false) }
                >
                  <img src="/images/cro.svg" className="h-7 w-7" />
                </button>
                <ReactPlayer
                  width="100%"
                  controls={ true }
                  url={ videoContents[0]?.video_url }
                />
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>
      {/* End Madmodal */ }
      { pageloader ? <Pageloader /> : null }
      {/* Start Banner Section */ }

      <Slider { ...silder1 } className="slider1 relative">
        { sliderdata?.map((ele, index) =>
        {
          return (
            <div className="banner_silder_item relative" key={ index + "" }>
              <img
                src={ ele?.slider_image }
                className="h-full w-full object-cover "
              />
              <div className="banner_item_text pl-129px flex flex-col justify-center lg:px-40px xmd:px-20px top-0 left-0 absolute z-20">
                <h1 className="text-white font33 font-bold">
                  { ele?.slider_title }
                </h1>
                <p className="text-white text-lg font-thin py-6 w-445px sm:w-full">
                  { ele?.slider_description }
                </p>
                { ele?.slider_link ? (
                  <div className="flex">
                    <Link href={ `/${ele?.slider_link}` }>
                      <a className="h-34px w-99px bg-liteblue cursor-pointer text-white text-sm mt-3 mr-3 flex justify-center items-center">
                        View more
                      </a>
                    </Link>
                    <button
                      className="h-34px w-99px border border-liteblue bg-transparent text-liteblue text-xs mt-3"
                      onClick={ () => router.push("/contact") }
                    >
                      Contact us
                    </button>
                  </div>
                ) : null }
              </div>
            </div>
          );
        }) }
      </Slider>

      {/* End Banner Section */ }
      {/* Start Event Secton */ }
      <div className="py-4 border-b hidden my-4">
        <div className="customcontainer hidden mx-auto flex items-center xmd:block ">
          <h1 className="text-3xl font-bold  text-black py-4 w-232px ">
            News &<span className="text-liteblue "> Events </span>
          </h1>
          <div className="flex border-l pl-5 xmd:p-0 xmd:border-0 xmd:flex-wrap  flex-grow justify-between">
            <div>
              <h3 className="text-black text-lg font-semibold">
                { eventlist?.event_date }
              </h3>
              <p className="text-sm text-litegray">
                { eventlist?.event_excerpt }
              </p>
            </div>
            <Link
              href={ `/events/${[eventlist?.event_slug]}/${[
                eventlist?.event_id,
              ]}` }
              as={ `/events/${[eventlist?.event_slug]}/${[eventlist?.event_id]}` }
              prefetch={ false }
            >
              <a className="h-34px w-99px border border-liteblue text-liteblue text-sm mt-3 flex justify-center items-center">
                Read more
              </a>
            </Link>
          </div>
        </div>

      </div>
      <div className="customcontainer mx-auto mt-10 sm:hidden">
        <h1 className="text-3xl font-bold text-black text-center">
          <span className="text-black">Our </span> Partners
        </h1>
        <div className={ "grid grid-cols-4" }>
          {
            reviews?.map((element, index) => (
              <Customer
                key={ index }
                text={ element?.review_content }
                image={ element?.review_image }
                Customer={ element?.review_name }
                designation={ element?.review_designation }
              />
            ))
          }
        </div>
      </div>
      {/* End Event Secton */ }
      {/* Start Machine Offer Section */ }
      <div className="bg-whiteghost mt-6 relative w-full">
        <img
          src="images/offer.svg"
          className="absolute right-2 top-28 xmd:hidden hidden"
          alt="image"
        />
        <div className="customcontainer mx-auto flex items-center sm:items-start sm:flex-col py-10 ">
          <h1 className="text-3xl font-bold hidden sm:block text-left  text-black py-4">
            { weoffer?.theme_weoffer_title }
          </h1>
          <div className="w-1/2 sm:w-full">
            <img className="rounded-xl" src={ weoffer?.theme_weoffer_image } />
          </div>
          <div className="w-1/2 sm:w-full sm:mt-4 ml-5 pr-3 break-all">
            <h1 className="text-3xl font-bold text-black py-4 sm:hidden">
              { weoffer?.theme_weoffer_title }
            </h1>
            <p style={ { wordBreak: 'break-word' } } className="text-sm text-justify text-litegray py-4">
              { parse(weoffer?.theme_weoffer_content) }
            </p>
            <Link href={ `${weoffer?.theme_weoffer_link}` }>
              <a className="bg-liteblue text-sm text-white w-150px h-34px flex justify-center items-center">
                Explore
              </a>
            </Link>
          </div>
        </div>
      </div>
      {/* End Machine Offer Section */ }
      {/* start categoier Section */ }
      <div className="customcontainer mx-auto mt-14">
        <h1 className="text-black text-3xl font-semibold">Categories</h1>
        <div className=" flex flex-wrap mt-8 items-start">
          <div className=" bg-liteblue tablist w-1/5 sm:w-full">
            { categoierlist?.map((ele, index) =>
            {
              return (
                <div>
                  <div className={ `border-b py-1 cursor-pointer flex items-center  ${selectedTab === ele
                    ? "bg-white  text-liteblue"
                    : "bg-liteblue"
                    } ` } key={ index + "" }>
                    <div
                      onClick={ () =>
                      {
                        setSelectedSubCategoryId(null)
                        setSelectedCategoryImages(ele?.category_image);
                        setSelectedTab(ele);
                        setSelectedCaetgoryId(ele?.category_id);
                        setIsSubCategoryOpened(true)
                        setSelectedSubCategoriesList(ele?.subcatgories)
                      } }
                      className={ `py-1 pl-8 text-left text-white text-base w-full flex cursor-pointer` }
                    >
                      <span className={ ` ${selectedTab === ele
                        ? " text-liteblue"
                        : "text-white"
                        }` }> { ele.category_name }</span>
                    </div>
                    <div className="flex-1"></div>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6 "
                      viewBox="0 0 20 20"
                      fill={ `${selectedTab === ele ? '#00A1EB' : 'white'}` }
                    >
                      <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10
                     7.293 6.707a1 1 0 011.414-1.414l4 
                    4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                  </div>
                  { selectedSubCategoriesList?.length > 0 && selectedTab === ele ? <div>
                    { selectedSubCategoriesList.map((element, index) =>
                    {
                      return (
                        <div
                          className="border-b-2 py-2 cursor-pointer bg-white"
                          key={ index + "" }
                          onClick={ () =>
                          {
                            setSelectedSubCategoryId(element?.subcategory_id)

                          } }
                        >
                          <h1 className='text-textGray pl-11 text-sm'>{ element?.subcategory_name }</h1>
                        </div>
                      )
                    })
                    }
                  </div> : null }
                </div>
              );
            }) }
          </div>
          <div className="tabcontants  w-4/5 pl-8 sm:w-full sm:pl-0 sm:mt-9 relative overflow-hidden self-stretch min-h-full">
            <div className="w-full grid grid-cols-3 gap-4 ">
              { productsList?.map((item, index) =>
              {
                return (
                  <Link
                    href={ `/product/${[item?.slug]}/${[item?.id]}` }
                    as={ `product/${[item?.slug]}/${[item?.id]}` }
                    key={ index }
                  >
                    <div className="flex justify-between flex-wrap">
                      <div
                        onClick={ () => setcards(item) }
                        className="categoier_card w-238px mb-8"
                      >
                        <div
                          className={ ` ${cards === item ? "categoier_img_active" : ""
                            }` }
                        >
                          <img
                            src={ item?.image }
                            className="h-52 w-full object-contain "
                          />
                        </div>
                        <h4
                          className={ `text-lg text-black text-center mt-3  font-normal ${cards === item ? "text-liteblue" : ""
                            }` }
                        >
                          { item?.name }
                        </h4>
                      </div>
                    </div>
                  </Link>
                );
              }) }
            </div>
            <div className="text-center pt-6">
              { categoierlist?.length < 10 ? null : <button className="h-34px w-99px border border-liteblue text-liteblue
               text-base mt-3">
                View all
              </button> }
            </div>

          </div>
        </div>
      </div>
      {/* End categoier Section */ }

      {/* Start progessive section */ }
      <div className="progessive_section mt-16 sm:mt-6 relative">
        <img
          src="images/khalid.svg"
          className="absolute right-8 z-20 top-28 lg:right-2 xmd:hidden"
        />
        <h1 className="text-3xl font-bold  text-liteblue mb-5 hidden sm:block px-20px">
          { videoContents[0]?.first_video_title }
          <span className="text-liteblue hidden">
            { videoContents[0]?.second_video_title }
          </span>
        </h1>
        <div className="customcontainer mx-auto flex sm:flex-col-reverse items-start py-10">
          <div className="w-1/2 pr-8 sm:w-full sm:p-0">
            <h1 className="text-3xl font-bold  text-black mb-5 sm:hidden">
              { videoContents[0]?.first_video_title }
              <span className="text-liteblue hidden">
                { videoContents[0]?.second_video_title }
              </span>
            </h1>
            <div className="text-justify">{ parse(videoContents[0]?.videosection_content) }</div>
          </div>
          <div className="w-1/2 relative sm:w-full sm:mb-5">
            <img src={ videoContents[0]?.video_cover } className="sm:w-full" />
            <button
              className="absolute top-40 left-56 lg:left-202px lg:top-115px xmd:left-150px xmd:top-99px sm:top-181px sm:left-238px cursor-pointer
              xs:top-99px xs:left-150px xxs:left-110px xxs:top-82px"
              onClick={ () => setOpen(true) }
            >
              <img src="images/ply.svg" />
            </button>
          </div>
        </div>
      </div>
      {/* End progessive section */ }
      {/* Start company About */ }
      <div className="customcontainer mx-auto border-t flex sm:flex-wrap justify-between pt-7 ">
        <div className="w-310px sm:w-full sm:mb-20px break-all">
          <h6 className="text-liteblue text-base">20 Plus Years</h6>
          <h2 className="text-3xl font-bold text-black py-3 text-justify">{ videoContents[0]?.firstsection_title }</h2>
          <p style={ { wordBreak: 'break-word' } } className="text-litegray text-justify text-sm ">
            { videoContents[0]?.firstsection_content }
          </p>
        </div>
        <div className="w-310px sm:w-full sm:mb-20px break-all">
          <h6 className="text-liteblue text-base">Multifunctional Hi-tech</h6>
          <h2 className="text-3xl font-bold text-black py-3">{ videoContents[0]?.secondsection_title }</h2>
          <p style={ { wordBreak: 'break-word' } } className="text-litegray text-sm text-justify">
            { videoContents[0]?.secondsection_content }
          </p>
        </div>
        <div className="w-310px sm:w-full sm:mb-20px break-all">
          <h6 className="text-liteblue text-base">Experts</h6>
          <h2 className="text-3xl font-bold text-black py-3">{ videoContents[0]?.thirdsection_title }</h2>
          <p style={ { wordBreak: 'break-word' } } className="text-litegray text-sm text-justify">
            { videoContents[0]?.thirdsection_content }

          </p>
        </div>
      </div>
      {/* End company About */ }
      {/* Start our company section */ }
      <div className="bg-whiteghost py-40px mt-20 sm:mt-10 relative">
        <img
          src="images/our_companies.png"
          className="h-96 absolute right-2 top-40 xmd:hidden"
        />
        <div className="customcontainer mx-auto">
          <h1 className="text-3xl font-bold  text-black">
            Our Group
            <span className="text-black "> Of Companies </span>
          </h1>
          <div className="flex mt-50px items-start sm:flex-col-reverse">
            <div className="w-1/2 sm:w-full">
              <p className="text-litegray text-justify text-sm">
                { themContents?.themecontent_about }
              </p>
              <div className="grid grid-cols-2 mt-7">
                { list.map((ele, index) =>
                {
                  return (
                    <div className="flex items-center mb-4 py-2" key={ index + "" }>
                      <img src="images/right.svg" alt="" />
                      <span className="text-litegray text-sm ml-8">
                        { ele.name }
                      </span>
                    </div>
                  );
                }) }
              </div>
              <div className="mt-40px flex flex-wrap hidden">
                <div className="w-1/2">
                  <h1 className="text-3xl text-liteblue font-bold">85%</h1>
                  <p className="text-litegray text-sm">
                    Rawal Industrial Equipment
                  </p>
                </div>
                <div className=" w-1/2">
                  <h1 className="text-3xl text-liteblue font-bold">93%</h1>
                  <p className="text-litegray text-sm">Frontier Ceramics</p>
                </div>
                <div className=" w-1/2 mt-4">
                  <h1 className="text-3xl text-liteblue font-bold">96%</h1>
                  <p className="text-litegray text-sm">Toyota Rawal Motors</p>
                </div>
                <div className=" w-1/2 mt-4">
                  <h1 className="text-3xl text-liteblue font-bold">98%</h1>
                  <p className="text-litegray text-sm">
                    Khalid & Khalid Holdings
                  </p>
                </div>
              </div>
            </div>
            <div className="w-1/2 flex justify-end items-end sm:w-full sm:block sm:mb-20px">
              <div className="relative">
                <img
                  src={ themContents?.themecontent_about_image }
                  alt=""
                  className="sm:w-full sm:h-504px sm:object-cover sm:object-center"
                />
                <button className="bg-liteblue absolute bottom-0 h-50px w-full text-white text-base flex justify-around items-center " onClick={ () => router.push("/contact") } >
                  Get quote now <img src="images/rightw.svg" alt="" />
                </button>
              </div>
            </div>

          </div>
          {/* Logos */ }
          <div className="flex justify-between  mt-10 space-x-7">
            { compinesLogo?.map((ele, index) =>
            {

              return (
                <div key={ index + "" } >
                  <a href={ ele?.company_link } target="_blank" rel="noopener noreferrer">
                    <img
                      key={ index }
                      src={ ele?.company_logo }
                      alt=""
                      className="mx-1 w-20 h-20 rounded  cursor-pointer"
                    />
                  </a>
                </div>
              );
            }) }
          </div>
        </div>
      </div>
      {/* End our company section */ }
      {/* Start KKH Manufacturers section */ }
      <div className="customcontainer mx-auto mt-50px">
        <h1 className="text-3xl font-bold  text-black">
          Products
        </h1>
        <div className="xmd:overflow-x-scroll scroll">
          <div className="mt-45px grid grid-cols-3 gap-x-8 menufacture_card">
            { cardsList?.map((item, index) => (
              <Card
                key={ index }
                className="w-269px xmd:w-238px  "
                img={ item?.image }
                title={ item?.name }
                text={ item?.details }
                btntext="Read More"
                btnclass="text-liteblue"
                href={ `product/${[item?.slug]}/${[item?.id]}` }
                as={ `product/${[item?.slug]}/${[item?.id]}` }
              />
            )) }
          </div>
        </div>
      </div>
      {/* End KKH Manufacturers section */ }
      {/* Start Reach with Us Section*/ }
      <div className="bg-whiteghost">
        <div className=" pl-129px py-8 mt-99px relative lg:pl-40px sm:px-20px 2xl:w-1088px 2xl:mx-auto 2xl:px-0">
          <h1 className="text-3xl font-bold text-black ">
            <span className="text-black ">Reach </span> Us
          </h1>
          <p className="italic text-base text-darkgray font-medium mt-7">
            we are ready 24/7 hours to support.
          </p>
          <img
            src="images/mac.png"
            alt=""
            className="absolute right-16 -top91px lg:right-0 xmd:hidden 2xl:right-0"
          />
          <form>
            <div>
              <input
                onChange={ (e) => setUserName(e.target.value) }
                type="text"
                placeholder="Enter your name"
                className="text-sm text-litegray pl-3 my-4 w-445px h-56px sm:w-full"
              />
            </div>
            <div>
              <input
                onChange={ (e) => setUserEmail(e.target.value) }
                type="email"
                required
                placeholder="Enter your email"
                className="text-sm text-litegray pl-3 my-4 w-445px h-56px sm:w-full"
              />
            </div>
            <div>
              <textarea
                onChange={ (e) => setUserMassage(e.target.value) }
                placeholder="Enter your message"
                className="text-sm text-litegray resize-none p-3 my-4 w-643px h-110px lg:w-504px sm:w-full"
              />
            </div>
            <button
              className="bg-liteblue text-sm text-white w-150px h-34px"
              onClick={ (e) =>
              {
                e.preventDefault();
                submitForm();
              } }
            >
              Send message
            </button>
          </form>
        </div>
      </div>
      {/* End Reach with Us Section*/ }
      {/* Start Adv card Section */ }
      <div className="xmd:overflow-x-scroll scroll ">
        <div className="customcontainer mx-auto mt-50px">
          <h1 className="text-3xl font-bold text-black ">
            Latest
            <span className="text-black"> News</span>
          </h1>
          <div className="mt-7 flex justify-between cardscroll">
            { blogCardsList?.map((element, index) => (
              <Card
                key={ index }
                className="w-232px"
                img={ element.cover }
                title={ element.title }
                text={ element?.details }
                btntext="Read More"
                btnclass="text-liteblue"
                href={ `blog/${[element?.slug]}/${[element.code]}` }
                as={ `blog/${[element?.slug]}/${[element.code]}` }
              />
            )) }
          </div>

        </div>
      </div>
      {/* End Adv card Section */ }
      {/* Start Follow with us section */ }
      <div className="mt-50px sm:hidden">
        <h1 className="text-3xl font-bold text-black text-center">
          Follow
          <span className="text-black"> Us On </span>
        </h1>
        <div className="flex justify-center mt-5">
          <img
            src="images/ytube.svg"
            alt=""
            className="mr-9 h-77px w-77px cursor-pointer"
          />
          <a href={ `${weoffer?.theme_instagram}` }>
            <img
              src="images/pin.svg"
              alt=""
              className="mr-9 h-77px w-77px cursor-pointer"
            />
          </a>
          <a href={ `${weoffer?.theme_linkdin}` }>
            <img
              src="images/tiwt.svg"
              alt=""
              className="mr-9 h-77px w-77px cursor-pointer"
            />
          </a>

          <a href={ `${weoffer?.theme_facebook}` }>
            <img
              src="images/fb.svg"
              alt=""
              className="mr-9 h-77px w-77px cursor-pointer"
            />
          </a>

          <a href={ `${weoffer?.theme_instagram}` }>
            <img
              src="images/skp.svg"
              alt=""
              className="mr-9 h-77px w-77px cursor-pointer"
            />
          </a>
        </div>
      </div>
      {/* End Follow with us section */ }
    </>
  );
}

const loadBlogCards = async () =>
{
  try {
    const { data } = await Api.get("/get-blog-cards/4");
    return data;
  } catch (error) {
    console.log(error);
    return []
  }
};

const loadProductCardsList = async () =>
{
  try {
    const { data } = await Api.get("/get-product-cards/3");
    return data;
  } catch (error) {
    console.log(error);
    return []
  }
};

const eventapi = async () =>
{
  try {
    const { data } = await Api.get("/get-latest-single-event");
    return data[0];
  } catch (error) {
    return [];
  }
};

const categorie = async () =>
{
  try {
    const { data } = await Api.get("/categories-dropdown");
    return data;
  } catch (error) {

    return [];
  }
};

const sliderapi = async () =>
{
  const { data } = await Api.get("/get-sliders");
  return data;
};

const loadoffer = async () =>
{
  const { data } = await Api.get("/get-theme-information");
  return data;
};

const loadFooterLinks = async () =>
{
  const { data } = await Api.get("/get-footer-links");
  return data;
};

const loadVideoContents = async () =>
{

  const { data } = await Api.get('get-video-content')
  return data
}

const loadThemData = async () =>
{
  try {
    const { data } = await Api.get('get-theme-information');
    return data
  } catch (error) {
    return {}
  }
}

const loadCustomerReviews = async () =>
{
  try {
    const { data } = await Api.get('get-customer-reviews')
    return data
  } catch (error) {
    return []
  }
}

const loadThemContents = async () =>
{

  try {
    const { data } = await Api.get('get-theme-content')
    return data
  } catch (error) {
    return {}
  }

}



export async function getServerSideProps (context)
{
  const blogCardsList = await loadBlogCards();
  const cardsList = await loadProductCardsList();
  const eventlist = await eventapi();
  const categoierlist = await categorie();
  const sliderdata = await sliderapi();
  const footerLink = await loadFooterLinks();
  const weoffer = await loadoffer();
  const videoContents = await loadVideoContents()
  const themData = await loadThemData()
  const reviews = await loadCustomerReviews()
  const themContents = await loadThemContents()

  return {
    props: {
      blogCardsList,
      cardsList,
      eventlist,
      categoierlist,
      sliderdata,
      weoffer,
      footerLink,
      videoContents,
      themData,
      reviews,
      themContents
    },
  };
}
