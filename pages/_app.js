import { useState } from "react";
import "../styles/globals.css";
import "../styles/main.scss";
import "tailwindcss/tailwind.css";
import "../styles/font.css";
import dynamic from "next/dynamic";

function MyApp ({ Component, pageProps })
{
  const Topbar = dynamic(() => import("../components/topbar/topheader"));
  const Footer = dynamic(() => import("../components/footer/footer"));
  return (
    <>
      <Topbar />
      <Component { ...pageProps } />
      <Footer />
    </>
  );
}

export default MyApp;
