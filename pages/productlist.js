import React, { useState, useEffect } from "react";
import Card from "../components/cards/cards";
import Pageloader from "../components/pageloader/pageloader";
import Api from "./api/api";

const Productlist = () =>
{
  const [pageLoader, setPageLoader] = useState(false);
  const [categoiresList, setCategoiresList] = useState([]);
  const [productId, setProductId] = useState();
  const [productsList, setProductsList] = useState([]);
  const [subCategoryId, setsubCategoryId] = useState('');
  const [data, setdata] = useState({});

  const categorie = async () =>
  {
    try {
      setPageLoader(true);
      const { data } = await Api.get("/categories-dropdown");
      setCategoiresList([...data]);
      setProductId(data[0]?.category_id);
      setPageLoader(false);
    } catch (error) {
      setPageLoader(false);
      console.log(error);
    }
  };
  const loadProductList = async () =>
  {
    try {
      setPageLoader(true);
      const { data } = await Api.get(`/category-wise-products/${productId}/${subCategoryId}`);
      setProductsList([...data]);
      setPageLoader(false);
    } catch (error) {
      setPageLoader(false);
      console.log(error);
    }
  };



  const loadThemData = async () =>
  {
    try {
      const { data } = await Api.get('get-theme-content')
      setdata(data)
    } catch (error) {
      console.log(error)
    }
  }



  useEffect(() =>
  {
    loadThemData()

  }, []);


  useEffect(() =>
  {
    categorie();
  }, []);
  useEffect(() =>
  {
    loadProductList();
  }, [productId, subCategoryId]);
  return (
    <>
      { pageLoader ? <Pageloader /> : null }
      <div className="bg_banner productlist_bg px-129px py-14 lg:px-40px lg:py-5 xmd:px-5">
        <h1 className="text-white text-3xl font-semibold">Product </h1>
        <h3 className="text-white font22 mt-3">Explore our shop</h3>

        <p className="text-white text-base font-light my-3 sm:hidden">
          { data?.themecontent_products }
        </p>
      </div>
      <div className="customcontainer flex mx-auto mt-12 sm:flex-col ">
        <div className=" w-1/4 sm:w-full">
          <div className="bg-whiteghost px-4 py-5">
            <h1 className="text-xl text-black font-semibold">Product Categories</h1>
            <div className="mt-4">
              { categoiresList?.map((item, index) =>
              {
                return (
                  <div>
                    <div
                      className="flex justify-between items-center border-t w-full py-2.5 cursor-pointer"
                      key={ index + "" }
                      onClick={ () =>
                      {
                        setsubCategoryId('')
                        setProductId(item?.category_id)
                      }
                      }
                    >
                      <strong className="text-base text-black">
                        { item?.category_name }
                      </strong>
                    </div>
                    {
                      productId == item?.category_id && <div>
                        {
                          item?.subcatgories?.map((ele, index) =>
                          {
                            return (
                              <div
                                className="mb-2.5 cursor-pointer transition-all duration-500"
                                key={ index + "" }
                                onClick={ () => setsubCategoryId(ele?.subcategory_id) }
                              >
                                <h1 className="text-sm text-black font-semibold">{ ele?.subcategory_name }</h1>
                              </div>
                            )
                          })
                        }
                      </div>
                    }
                  </div>
                );
              }) }
            </div>
          </div>
        </div>
        <div className="w-3/4 grid  pl-7 grid-cols-3  gap-6 xmd:grid-cols-2 sm:grid-cols-1 sm:w-full sm:pl-0 sm:mt-8">
          { productsList.map((ele, index) =>
          {
            return (
              <Card
                key={ index }
                className="w-250px lg:w-52 border p-2 mb-7 xs:w-full "
                img={ ele?.image }
                title={ ele?.name }
                href={ `product/${[ele?.slug]}/${[ele?.id]}` }
                as={ `product/${[ele?.slug]}/${[ele?.id]}` }
                btntext="View product"
                btnclass="w-150px bg-liteblue text-white"
              />
            );
          }) }
        </div>
      </div>
    </>
  );
};

export default Productlist;
