import React, { useState, useRef, Fragment, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import Customer from "../components/customercard/customer";
import Api from './api/api'

const Aboutus = () =>
{
  const cancelButtonRef = useRef(null);
  const [selectedCertificate, setSelectedCertificate] = useState('');

  const [open, setOpen] = useState(false);
  const [data, setdata] = useState({});
  const [certificatesList, setCertificatesList] = useState([]);

  const loadThemData = async () =>
  {
    try {
      const { data } = await Api.get('get-theme-content')
      setdata(data)
    } catch (error) {
      console.log(error)
    }
  }

  const loadCertificates = async () =>
  {
    try {
      const { data } = await Api.get('get-company-certificates')
      setCertificatesList([...data])
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() =>
  {
    loadThemData()
    loadCertificates()
  }, []);

  return (
    <>
      <Transition.Root show={ open } as={ Fragment }>
        <Dialog
          as="div"
          className="fixed z-10 inset-0 overflow-y-auto"
          initialFocus={ cancelButtonRef }
          onClose={ setOpen }
        >
          <div className="flex items-start justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <Transition.Child
              as={ Fragment }
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */ }
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={ Fragment }
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <div className="inline-block align-middle pb-3 w-643px h-435px xmd:w-full bg-white text-left overflow-hidden shadow-xl transform transition-all px-5">
                <button
                  className="h-7 w-7 ml-auto block my-3 mr-2"
                  onClick={ () => setOpen(false) }
                >
                  <img src='/images/cro.svg' className="h-7 w-7" />
                </button>
                <div>
                  <img src={ selectedCertificate } className="w-full" />
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>
      {/* End Madmodal */ }
      <div className="bg_banner px-129px py-20 lg:px-40px lg:py-5 xmd:px-5">
        <h1 className="text-white text-3xl font-semibold">About Us</h1>
        <p className="text-white text-lg font-light my-3 text-justify sm:hidden">
          { data?.themecontent_about }
        </p>

      </div>
      {/* Start Our vesion Section */ }
      <div className="customcontainer mx-auto flex mt-50px sm:flex-col-reverse">
        <div className="w-1/2 sm:w-full">
          <h1 className="text-3xl font-bold text-black sm:mt-3">
            Our
            <span className="text-black "> Vision</span>
          </h1>
          <p className="text-base text-justify text-litegray my-10 sm:my-3">
            { data?.themecontent_vision }
          </p>

        </div>
        <div className="w-1/2 text-right sm:w-full sm:text-left">
          <img src="images/aboutus/vision.jpeg" className="inline-block h-96 rounded" />
        </div>
      </div>
      {/* End Our vesion Section */ }
      {/* Start Our mission Section */ }
      <div className="customcontainer mx-auto flex flex-wrap mt-50px">
        <div className="w-1/2 sm:w-full p-4">
          <img
            src="images/aboutus/mission.jpeg"
            className="rounded"
            alt='missionimage'
          />
        </div>
        <div className="w-1/2 sm:w-full">
          <h1 className="text-3xl font-bold text-black sm:mt-3">
            Our
            <span className="text-black "> Mission</span>
          </h1>
          <p className="text-base text-justify text-litegray my-10 sm:my-3">
            { data?.themecontent_mission }
          </p>
        </div>
      </div>
      <div className="customcontainer mt-10 mx-auto">
        <h1 className="text-3xl font-bold text-black">
          Our
          <span className="text-black "> Commitment</span>
        </h1>

        <p className="text-litegray lg:w-2/3 xxs:w-full xss:w-full text-justify text-base my-6">
          Our commitment is to provide logistic solutions that are cost effective without any compromise in quality and performance. Khalid & Khalid Holdings devotes itself to nation building and contributes towards Pakistan's economic growth. We focus to maintain higher standards which clearly show how we value our business.
        </p>

      </div>
      <div className="customcontainer mt-10 mx-auto">
        <h1 className="text-3xl font-bold text-black">
          Our
          <span className="text-black "> Values</span>
        </h1>

        <p className="text-litegray text-base lg:w-2/3 xxs:w-full xss:w-full text-justify my-6">
          We believe in upholding the ethical principles of ISLAM that create equality, jusitice, fairness brotherhood, Integrity and transparency in all aspects of business interaction.
        </p>

      </div>
      {/* End Our mission Section */ }
      <div className="bg-whiteghost py-14 mt-40px">
        {/* Start Our achievements */ }
        <div className="customcontainer mx-auto">
          <h1 className="text-3xl font-bold text-black">
            Our
            <span className="text-black "> Achievements</span>
          </h1>

          <p className="text-litegray text-sm my-6 hidden">
            { data?.themecontent_achivement }
          </p>

        </div>
        {/* End Our achievements */ }
        <section className="customcontainer mx-auto mt-8 grid grid-cols-3 gap-x-14 gap-y-5 lg:gap-x-10 xmd:grid-cols-2 sm:grid-cols-1">
          {
            certificatesList?.map((ele, index) =>
            {
              return (
                <div
                  className="border-2 p-0.5 w-80  border-liteblue achivment__cards lg:w-72"
                  key={ index }
                >
                  <img
                    src={ ele?.certificate_file }
                    className="w-full h-40 object-cover object-top"
                    alt="certificatefile"
                  />
                  <div className="achivment__cards_icon">
                    <button onClick={ () =>
                    {
                      setSelectedCertificate(ele?.certificate_file)
                      setOpen(true)
                    } }>
                      <img src="images/eye.svg" alt="eyeimg" />
                    </button>
                  </div>
                </div>

              )
            })
          }
        </section>
      </div>

    </>
  );
};

export default Aboutus;
