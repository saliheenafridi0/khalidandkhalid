import React, { useState, useEffect, useRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Card from "../../../../components/cards/cards";
import { useRouter } from "next/router";
import Link from "next/link";
import Api from "../../../api/api";

const Prev = ({ onClick }) =>
{
  return (
    <button
      type="button"
      data-role="none"
      className="slick-arrow slick-prev"
      onClick={ onClick }
    >
      <img src="/images/leftgr.svg" className="transform rotate-180" />
    </button>
  );
};
const Next = ({ onClick }) =>
{
  return (
    <button
      type="button"
      data-role="none"
      className="slick-arrow slick-next"
      onClick={ onClick }
    >
      <img src="/images/rightgr.svg" />
    </button>
  );
};

const Index = () =>
{
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const slider1 = useRef(null);
  const slider2 = useRef(null);
  const [tab, settab] = useState("tabs1");
  const [detailsData, setDetailsdata] = useState({});
  const [relatedProductstList, setrelatedProductList] = useState([]);

  const router = useRouter();

  const loadRelatedProductsList = async (categoryId) =>
  {
    try {
      const { data } = await Api.get(`/category-wise-products/${categoryId}`);
      setrelatedProductList([...data]);
    } catch (error) { }
  };

  const loadDetails = async () =>
  {
    const id = router?.query?.id;
    const slug = router?.query?.slug
    try {
      const { data } = await Api.get(`/product/${slug}/${id}`);

      loadRelatedProductsList(data[0]?.category_id);
      setDetailsdata({ ...data[0] });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() =>
  {
    loadDetails();
    setNav1(slider1.current);
    setNav2(slider2.current);
  }, []);
  return (
    <>
      <div className="bg-whiteghost">
        <div className="customcontainer mx-auto py-40px">
          <h1 className="text-black text-3xl font-semibold">
            { detailsData?.name }
          </h1>
          <Slider
            arrows={ false }
            asNavFor={ nav2 }
            ref={ slider1 }
            infinite={ false }
            className="items_slider"
          >
            { detailsData?.images?.map((item, index) => (
              <div key={ index }>
                <img src={ item?.product_image } />
              </div>
            )) }
          </Slider>
          <Slider
            asNavFor={ nav1 }
            ref={ slider2 }
            // slidesToShow={6}
            swipeToSlide={ true }
            focusOnSelect={ true }
            arrows={ true }
            nextArrow={ <Next /> }
            prevArrow={ <Prev /> }
            responsive={ [
              {
                breakpoint: 768,
                settings: {
                  arrows: false,
                },
              },
            ] }
            className="nav_slider"
          >
            { detailsData?.images?.map((item, index) =>
            {
              return (
                <div key={ index }>
                  <img src={ item?.product_image } />
                </div>
              );
            }) }
          </Slider>
        </div>
      </div>
      <div className="customcontainer mx-auto mt-8">
        <div className="bg-whiteghost flex">
          <button
            className={ `${tab === "tabs1" ? "bg-liteblue text-white" : ""
              } h-70px w-1/3 text-black text-lg font-semibold sm:text-sm xs:text-xs sm:h-50px transition  duration-300 ease-in-out` }
            onClick={ () => settab("tabs1") }
          >
            Description
          </button>
          <button
            className={ `${tab === "tabs2" ? "bg-liteblue text-white" : ""
              } h-70px w-1/3 text-black text-lg sm:text-sm xs:text-xs sm:h-50px font-semibold transition  duration-300 ease-in-out` }
            onClick={ () => settab("tabs2") }
          >
            Additional information
          </button>
          <button
            className={ `${tab === "tabs3" ? "bg-liteblue text-white" : ""
              } h-70px w-1/3 text-black text-lg sm:text-sm xs:text-xs sm:h-50px font-semibold transition  duration-300 ease-in-out` }
            onClick={ () => settab("tabs3") }
          >
            Downloads
          </button>
        </div>
        { tab === "tabs1" && (
          <div className="mt-8">
            <p className="text-sm text-litegray">{ detailsData?.details }</p>

            <div className="mt-9 hidden">
              <h6 className="text-black text-lg font-semibold">Features</h6>
              <ul className="mt-5">
                <li className="flex items-start my-2.5">
                  <img src="/images/dots.svg" />
                  <p className="text-litegray text-sm ml-4">
                    Features are not available in end point.
                  </p>
                </li>
              </ul>
            </div>
          </div>
        ) }
        { tab === "tabs2" && (
          <div className="mt-8">
            <table className="border w-full">
              {/* Basic informations start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Basic informations</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Model number</th>
                      <td className="border-b pl-5">{ detailsData?.basic_model_number }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Driving methode</th>
                      <td className=" pl-5">{ detailsData?.basic_driving_method }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Basic informations end */ }
              {/* Size parameter start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Size parameters</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Total length(mm)</th>
                      <td className="border-b pl-5">{ detailsData?.size_total_length }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Overall width(mm)</th>
                      <td className="border-b pl-5">{ detailsData?.size_overall_width }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Total height(mm)</th>
                      <td className="border-b pl-5">{ detailsData?.size_total_height }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Wheelbase(mm)</th>
                      <td className="border-b pl-5">{ detailsData?.size_wheel_base }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Suspension</th>
                      <td className="border-b pl-5">{ detailsData?.size_front_rear_suspension }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Wheel track(mm)</th>
                      <td className=" pl-5">{ detailsData?.size_wheel_track }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Size parameters end */ }
              {/* Qaulity parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Quality paramters</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Total weight (kg)</th>
                      <td className="border-b pl-5">{ detailsData?.quality_total_weight }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Total vehicle mass (kg)</th>
                      <td className="border-b pl-5">{ detailsData?.quality_total_vehicle_mass }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Model number</th>
                      <td className=" pl-5">{ detailsData?.basic_model_number }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Qaulity parameters ends */ }
              {/* Performance parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Performance parameters</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">The maximum speed (km/hr)</th>
                      <td className="border-b pl-5">{ detailsData?.performance_maximum_speed }</td>
                    </tr>

                    <tr className="h-14">
                      <th className=" border-r w-1/3">Minimum turning diameter (mm)</th>
                      <td className=" pl-5">{ detailsData?.performance_minimum_turning_diameter }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Performance parameters ends */ }

              {/* Engine parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Engine</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Model</th>
                      <td className="border-b pl-5">{ detailsData?.engine_engine_model }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Type</th>
                      <td className="border-b pl-5">{ detailsData?.engine_engine_type }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Bore diameter (mm)</th>
                      <td className="border-b pl-5">{ detailsData?.engine_bore_diameter }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Stroke (mm)</th>
                      <td className="border-b pl-5">{ detailsData?.engine_engine_stroke }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Engine displacement (cc)</th>
                      <td className="border-b pl-5">{ detailsData?.engine_engine_displacement }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Compression ratio</th>
                      <td className="border-b pl-5">{ detailsData?.engine_compression_ratio }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Rated power kW(ps)</th>
                      <td className="border-b pl-5">{ detailsData?.engine_rated_power }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Maximum torque/speed N:m/r/min</th>
                      <td className="border-b pl-5">{ detailsData?.engine_maximum_torque }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Fuel type</th>
                      <td className="border-b pl-5">{ detailsData?.engine_engine_fuel_type }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">rated speed r/min</th>
                      <td className=" pl-5">{ detailsData?.engine_rated_speed }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Engine parameters ends */ }

              {/* Transmission parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Transmission</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Model</th>
                      <td className="border-b pl-5">{ detailsData?.transmission_transmission_model }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Control methode</th>
                      <td className="border-b pl-5">{ detailsData?.transmission_control_method }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Speed ratio</th>
                      <td className=" pl-5">{ detailsData?.transmission_speed_ratio }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/*  Transmission parameters ends */ }

              {/* Clutch parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Transmission</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Type</th>
                      <td className="border-b pl-5">{ detailsData?.clutch_clutch_type }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Diameter (mm)</th>
                      <td className="border-b pl-5">{ detailsData?.clutch_clutch_diameter }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Manipulation methode</th>
                      <td className=" pl-5">{ detailsData?.clutch_manipulation_method }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/*  Clutch parameters ends */ }
              {/* Axle parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Axle</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Front axle</th>
                      <td className="border-b pl-5">{ detailsData?.axle_front_axle }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Rear axle</th>
                      <td className=" pl-5">{ detailsData?.axle_rear_axle }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Axle parameters ends */ }
              {/* Frame parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Frame</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Structure type</th>
                      <td className="border-b pl-5">{ detailsData?.frame_structure_type }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Standard size</th>
                      <td className=" pl-5">{ detailsData?.frame_standard_sizes }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Frame parameters ends */ }
              {/* Suspension parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Suspension</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Front suspension structure type</th>
                      <td className="border-b pl-5">{ detailsData?.suspension_front_suspension_structure }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Rear suspension structure type</th>
                      <td className=" pl-5">{ detailsData?.suspension_rear_suspension_structure }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Suspension parameters ends */ }
              {/*Tyres parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Tyres</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Model</th>
                      <td className=" pl-5">{ detailsData?.tyres_tyres_model }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/*Tyres parameters ends */ }
              {/* Braking system parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Breaking system</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Braking type</th>
                      <td className="border-b pl-5">{ detailsData?.braking_braking_type }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Driving brake</th>
                      <td className="border-b pl-5">{ detailsData?.braking_driving_brake }</td>
                    </tr>
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Parkiung brake</th>
                      <td className="border-b pl-5">{ detailsData?.braking_parking_brake }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Auxiliary brake</th>
                      <td className=" pl-5">{ detailsData?.braking_auxiliary_brake }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Braking system parameters ends */ }
              {/* Breaking system parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Steering device</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Model</th>
                      <td className=" pl-5">{ detailsData?.steering_steering_type }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/* Breaking system parameters ends */ }

              {/* Fuel tank parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Fuel tank</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Capicity</th>
                      <td className="border-b pl-5">{ detailsData?.fuel_capacity }</td>
                    </tr>

                    <tr className="h-14">
                      <th className=" border-r w-1/3">Material</th>
                      <td className=" pl-5">{ detailsData?.fuel_material }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/*Fuel tank parameters ends */ }

              {/* Cabin parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Cabin</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">

                    <tr className="h-14">
                      <th className=" border-r w-1/3">Type</th>
                      <td className=" pl-5">{ detailsData?.cabin_cabin_type }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/*Cabin parameters ends */ }

              {/* Electrical parameters start */ }
              <tr>
                <th className="w-1/5 border p-4 text-black">Electrical system</th>
                <td className="w-4/5 border  text-litegray text-sm">
                  <table className="w-full">
                    <tr className="h-14">
                      <th className="border-b border-r w-1/3">Vehical voltage</th>
                      <td className="border-b pl-5">{ detailsData?.electrical_vehicle_voltage }</td>
                    </tr>
                    <tr className="h-14">
                      <th className=" border-r w-1/3">Battery</th>
                      <td className=" pl-5">{ detailsData?.electrical_battery }</td>
                    </tr>
                  </table>
                </td>
              </tr>
              {/*Electrical system parameters ends */ }



            </table>
          </div>
        ) }
        { tab === "tabs3" && (
          <div className="mt-8 border">
            { detailsData?.files?.map((element, index) => (
              <div
                className="border-b py-5 px-6 flex items-center cursor-pointer"
                key={ index }
              >
                <img src="/images/pdf.svg" />

                <a href={ element.product_file } target="_blank" rel="noopener noreferrer"  >
                  <p className="text-base text-litegray pl-5">
                    { element.product_file }
                  </p>
                </a>
              </div>
            )) }
          </div>
        ) }
      </div>
      <div className="customcontainer mx-auto mt-14">
        <h1 className="text-3xl font-bold  text-black">
          Related
          <span className="text-black "> Products </span>
        </h1>
        <div className="xmd:overflow-x-scroll scroll ">
          <div className="flex justify-between flex-wrap mt-10 cardscroll">
            { relatedProductstList.map((item, index) => (
              <Card
                key={ index }
                className="w-250px border p-2 lg:w-232px "
                img={ item?.image }
                title={ item?.name }
                // text="With the largest privately owned fleet of heavy duty trucks of experience in the maintenance of trucks and heavy equipment."
                btntext="View product"
                btnclass="w-150px bg-liteblue text-white"
              />
            )) }
          </div>
        </div>
      </div>
    </>
  );
};

export default Index;
