import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Pageloader from "../../../../components/pageloader/pageloader";
import Api from "../../../api/api";

const Index = () => {
  const [eventdetail, setEventdetail] = useState({});
  const [pageloader, setPageloader] = useState(false);
  const router = useRouter();
  const { id }= router?.query;
  const { slug } = router?.query;

  const loadeventdet = async () => {
    try {
      setPageloader(true);
      if ( slug !== undefined ) {
        const { data } = await Api.get(
          `/get-event-details/${slug}/${id}`
        );
          setEventdetail({ ...data });
          setPageloader(false);
        }
    } catch (error) {
      setPageloader(false);
      console.log(error);
    }
  };
  useEffect(() => {
    loadeventdet();
  }, []);
  return (
    <>
      {pageloader ? <Pageloader /> : null}
      <section className="customcontainer mx-auto mt-10">
        <img
          src={eventdetail[0]?.event_cover}
          className="w-full h-96 object-cover"
        />
        <h1 className="text-black text-3xl font-semibold mt-5">
          {eventdetail[0]?.event_title}
        </h1>
        <div className=" py-2 flex">
          <p className="text-sm text-litegray">{eventdetail[0]?.event_time}</p>
          <p className="text-sm text-litegray ml-4">
            {eventdetail[0]?.event_date}
          </p>
        </div>
        <p className="text-sm text-litegray py-4">
          {eventdetail[0]?.event_description.split(">")[1].split("<")[0]}
        </p>
      </section>
    </>
  );
};

export default Index;
